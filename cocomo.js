/* datos constantes, como los coeficientes o los porcentajes */
const data = {
    "PMnominalFactors": {
        'S': [3.2, 1.05],
        'M': [3, 1.12],
        'L': [2.8, 1.2]
    },

    /* Valores para la distribución de esfuezo */
    "yieldDistribution": {
        /* Orgánico */
        'S': [
            /* Requisitos, Diseño, Diseño Detallado, Codificación-Test, Integración */
            [.06, .1, .26, .42, .16],
            [.06, .1, .25, .40, .19],
            [.06, .1, .24, .38, .22],
            [.06, .1, .23, .36, .25],
            [0, 0, 0, 0, 0]
        ],

        /* Semiacoplado */
        'M': [
            /* Requisitos, Diseño, Diseño Detallado, Codificación-Test, Integración */
            [.07, .1, .27, .37, .19],
            [.07, .1, .26, .35, .22],
            [.07, .1, .25, .33, .25],
            [.07, .1, .24, .31, .28],
            [.07, .1, .23, .29, .31]
        ],

        /* Empotrado */
        'L': [
            /* Requisitos, Diseño, Diseño Detallado, Codificación-Test, Integración */
            [.08, .1, .28, .32, .22],
            [.08, .1, .27, .30, .25],
            [.08, .1, .26, .28, .28],
            [.08, .1, .25, .26, .31],
            [.08, .1, .24, .24, .34]
        ]
    },

    /* Multiplicadores a nivel de módulo (valor de EMi) */
    "questionFactor": {
        3: [.7, .85, 1, 1.15, 1.3, 1.65],
        10: [1.42, 1.17, 1, .86, .70],
        11: [1.21, 1.1, 1, .9],
        12: [1.14, 1.07, 1, .95]
    },

    /* Multiplicadores a nivel de módulo por fase */
    "questionPhaseFactor":[
        /* Fase del diseño del producto (DP) */
        {
            3: [.7, .85, 1, 1.15, 1.3, 1.65],
            10: [1, 1, 1, 1, 1],
            11: [1.1, 1.05, 1, .9],
            12: [1.02, 1, 1, 1]
        },

        /* Fase del Diseño Detallado (DD) */
        {
            3: [.7, .85, 1, 1.15, 1.3, 1.65],
            10: [1.5, 1.2, 1, .83, .65],
            11: [1.1, 1.05, 1, .9],
            12: [1.1, 1.05, 1, .98]
        },

        /* Fase de Codificación-Test (CT) */
        {
            3: [.7, .85, 1, 1.15, 1.3, 1.65],
            10: [1.5, 1.2, 1, .83, .65],
            11: [1.3, 1.15, 1, .9],
            12: [1.2, 1.1, 1, .92]
        },

        /* Fase de Integración Pruebas (IT) */
        {
            3: [.7, .85, 1, 1.15, 1.3, 1.65],
            10: [1.5, 1.2, 1, .83, .65],
            11: [1.3, 1.15, 1, .9],
            12: [1.2, 1.1, 1, .92]
        },
    ],

    "questionsSubsystem": [
        /* Fase del diseño del producto (DP) */
        {
            1: [.8, .9, 1, 1.1, 1.3],
            2: [0, .95, 1, 1.1, 1.2],
            4: [0, 0, 1, 1.1, 1.3, 1.65],
            5: [0, 0, 1, 1.05, 1.2, 1.55],
            6: [0, .95, 1, 1.1, 1.2],
            7: [0, .98, 1, 1, 1.02],
            8: [1.8, 1.35, 1, .75, .55],
            9: [1.4, 1.2, 1, .87, .75],
            13: [1.05, 1, 1, 1, 1],
            14: [1.02, 1, 1, .98, .95],
            15: [1.1, 1, 1, 1.1, 1.15]
        },

        /* Fase del diseño detallado (DD) */
        {
            1: [.8, .9, 1, 1.1, 1.3],
            2: [0, .95, 1, 1.05, 1.1],
            4: [0, 0, 1, 1.1, 1.25, 1.55],
            5: [0, 0, 1, 1.05, 1.15, 1.45],
            6: [0, .9, 1, 1.12, 1.25],
            7: [0, .95, 1, 1, 1.05],
            8: [1.35, 1.15, 1, .9, .75],
            9: [1.3, 1.15, 1, .9, .8],
            13: [1.1, 1.05, 1, .95, .9],
            14: [1.05, 1.02, 1, .95, .9],
            15: [1.25, 1.15, 1, 1.1, 1.15]
        },

        /* Fase de Codificación-Test (CT) */
        {
            1: [.8, .9, 1, 1.1, 1.3],
            2: [0, .95, 1, 1.05, 1.1],
            4: [0, 0, 1, 1.1, 1.25, 1.55],
            5: [0, 0, 1, 1.05, 1.15, 1.45],
            6: [0, .85, 1, 1.15, 1.3],
            7: [0, .7, 1, 1.1, 1.2],
            8: [1.35, 1.15, 1, .9, .75],
            9: [1.25, 1.1, 1, .92, .85],
            13: [1.25, 1.1, 1, .9, .8],
            14: [1.35, 1.15, 1, .9, .8],
            15: [1.25, 1.15, 1, 1, 1.05]
        },

        /* Fase de Codificación-Test (CT) */
        {
            1: [.6, .8, 1, 1.3, 1.7],
            2: [0, .9, 1, 1.15, 1.3],
            4: [0, 0, 1, 1.15, 1.4, 1.95],
            5: [0, 0, 1, 1.1, 1.35, 1.85],
            6: [0, .8, 1, 1.2, 1.4],
            7: [0, .9, 1, 1.15, 1.3],
            8: [1.5, 1.2, 1, .85, .7],
            9: [1.25, 1.1, 1, .92, .85],
            13: [1.5, 1.2, 1, .83, .65],
            14: [1.45, 1.2, 1, .85, .7],
            15: [1.25, 1.1, 1, 1, 1.05]
        }
    ]
};


const language = {
    es: {
        'programName': 'Calculadora de COCOMO avanzado'
        ,'subsystems': 'Subsistemas'
        ,'subsystem': 'Subsistema'
        ,'identifySubsystems': 'Identifica los subsistemas'
        ,'delete': 'Eliminar'
        ,'subsystemName': 'Nombre del subsistema'
        ,'add': 'Añadir'
        ,'modules': 'Módulos'
        ,'identifyModules': 'Identifica los módulos de cada subsistema'
        ,'moduleName': 'Nombre del módulo'
        ,'loc': 'Líneas de código'
        ,'totalLOC': 'líneas de código totales'
        ,'consideredSize': 'Consideramos el proyecto de tamaño'
        ,'consideredSize0': 'Elemental'
        ,'consideredSize1': 'Pequeño'
        ,'consideredSize2': 'Intermedio'
        ,'consideredSize3': 'Grande'
        ,'consideredSize4': 'Enorme'
        ,'devMode': 'Modo de desarrollo'
        ,'devModeChoose': 'Elige el modo de desarrollo'
        ,'devMode0': 'Modo orgánico: para proyectos pequeños'
        ,'devMode1': 'Modo semiacoplado: para proyectos medianos'
        ,'devMode2': 'Modo empotrado: para proyectos grandes'
        ,'effort%': '% Esfuerzo'
        ,'effort%0': 'Diseño del Producto (requisitos + diseño)'
        ,'effort%1': 'Diseño Detallado'
        ,'effort%2': 'Codificación-Test'
        ,'effort%3': 'Integración y Pruebas'
        ,'correctionFactors': 'Factores de corrección'
        ,'correctionFactorsSubtitle': 'Obtén los factores de corrección alterando los desplegables'
        ,'roundingWarning': 'Ten en cuenta que los valores que aparecen están redondeados, pero por dentro usamos hasta 14 decimales de precisión.'
        ,'CPLX': 'Complejidad'
        ,'PCAP': 'Capacidad de los programadores'
        ,'VEXP': 'Experiencia con la Máquina Virtual'
        ,'LEXP': 'Experiencia con el Lenguaje utilizado'
        ,'RELY': 'Confiabilidad requerida'
        ,'DATA': 'Tamaño de la base de datos'
        ,'TIME': 'Restricción del Tiempo de Ejecución'
        ,'STOR': 'Restricción del Almacenamiento Principal'
        ,'VIRT': 'Volatilidad de la Máquina Virtual'
        ,'TURN': 'Tiempo de Respuesta del Ordenador'
        ,'ACAP': 'Capacidad de los Analistas'
        ,'AEXP': 'Experiencia en aplicaciones similares'
        ,'MODP': 'Prácticas modernas en programación'
        ,'TOOL': 'Uso de herramientas software'
        ,'SCED': 'Cronograma de desarrollo requerido'
        ,'estimated': 'estimado'
        ,'EMoption0': 'Muy bajo'
        ,'EMoption1': 'Bajo'
        ,'EMoption2': 'Normal'
        ,'EMoption3': 'Alta'
        ,'EMoption4': 'Muy alta'
        ,'EMoption5': 'Extra alta'
        ,'EMattributeEvaluation': 'Evaluación de atributos EM'
        ,'EMattributeEvaluationSubtitle': 'Respondemos a las 11 preguntas restantes de cada subsistema'
        ,'finalResult': 'El Resultado Final'
        ,'finalResultSubtitle': 'Lo que estábamos esperando'
    },
    en: {
        'programName': 'Detailed COCOMO calculator'
        , 'subsystems': 'Subsystems'
        , 'subsystem': 'Subsystem'
        , 'identifySubsystems': 'Identify the subsystems'
        , 'delete': 'Delete'
        , 'subsystemName': 'Subsystem name'
        , 'add': 'Add'
        , 'modules': 'Modules'
        , 'identifyModules': 'Identify the modules of each subsystem'
        , 'moduleName': 'Module name'
        , 'loc': 'Lines of Code'
        , 'totalLOC': 'total Lines of Code'
        , 'consideredSize': 'We consider the project size to be'
        , 'consideredSize0': 'Tiny'
        , 'consideredSize1': 'Little'
        , 'consideredSize2': 'Intermediate'
        , 'consideredSize3': 'Big'
        , 'consideredSize4': 'Huge'
        , 'devMode': 'Development mode'
        , 'devModeChoose': 'Choose the development mode'
        , 'devMode0': 'Organic mode: for tiny and little projects'
        , 'devMode1': 'Semi-detached mode: for middle sized projects'
        , 'devMode2': 'Embedded mode: for big/huge projects'
        , 'effort%': '% Effort'
        , 'effort%0': 'Product design (requirements + design)'
        , 'effort%1': 'Detailed Design'
        , 'effort%2': 'Code-Test'
        , 'effort%3': 'Integration and Testing'
        , 'correctionFactors': 'Correction factors'
        , 'correctionFactorsSubtitle': 'Get the correction factors by modifying the dropdowns'
        , 'roundingWarning': 'Be aware: the values are rounded, but we are using up to 14 floating point precision'
        , 'CPLX': 'Complexity'
        , 'PCAP': "Programmers' Capacity"
        , 'VEXP': 'Experience with the Virtual Machine'
        , 'LEXP': 'Experiencia with the Language used'
        , 'RELY': 'Reliability required'
        , 'DATA': 'Database size'
        , 'TIME': 'Execution Time restriction'
        , 'STOR': 'Main Storage Restrictions'
        , 'VIRT': 'Virtual Machine volatibility'
        , 'TURN': "Computer's response time"
        , 'ACAP': 'Analysts capacity'
        , 'MODP': 'Prácticas modernas en programación'
        , 'AEXP': 'Experience in similar applications'
        , 'TOOL': 'Software tools usage'
        , 'SCED': 'Development Schedule Requirement'
        , 'estimated': 'estimated'
        , 'EMoption0': 'Very low'
        , 'EMoption1': 'Low'
        , 'EMoption2': 'Normal'
        , 'EMoption3': 'High'
        , 'EMoption4': 'Very High'
        , 'EMoption5': 'Extra High'
        , 'EMattributeEvaluation': 'EM attributes evaluation'
        , 'EMattributeEvaluationSubtitle': 'We answer the 11 remaining questions of each subsystem'
        , 'finalResult': 'Final Result'
        , 'finalResultSubtitle': 'What we were expecting'
    }
};

/* datos que varían en la calculadora */
var app = new Vue({
    el: '#app',
    data: {
        lang: 'es',
        newsubsystem: {},
        newmodule: {},
        subsystems: [
            {
                name: "Query",
                modules: [
                    {
                        name: "QEdit",
                        loc: 1800,
                        aaf: 100,
                        correction: {
                            3: 2,
                            10: 3,
                            11: 2,
                            12: 3
                        }
                    },{
                        name: "Search",
                        loc: 700,
                        aaf: 100,
                        correction: {
                            3: 3,
                            10: 4,
                            11: 2,
                            12: 3
                        }
                    },{
                        name: "Output",
                        loc: 1200,
                        aaf: 100,
                        correction: {
                            3: 1,
                            10: 2,
                            11: 2,
                            12: 2
                        }
                    }
                ],
                correction: {
                    1: 2,
                    2: 2,
                    4: 2,
                    5: 2,
                    6: 2,
                    7: 2,
                    8: 4,
                    9: 4,
                    13: 2,
                    14: 4,
                    15: 2
                }
            },{
                name: "Update",
                modules: [
                    {
                        name: "UpEdit",
                        loc: 1700,
                        aaf: 100,
                        correction: {
                            3: 2,
                            10: 3,
                            11: 2,
                            12: 3
                        }
                    },{
                        name: "Modify",
                        loc: 900,
                        aaf: 100,
                        correction: {
                            3: 1,
                            10: 2,
                            11: 2,
                            12: 2
                        }
                    }
                ],
                correction: {
                    1: 2,
                    2: 2,
                    4: 2,
                    5: 2,
                    6: 2,
                    7: 2,
                    8: 4,
                    9: 4,
                    13: 2,
                    14: 4,
                    15: 2
                }
            },{
                name: "Utilities",
                modules: [
                    {
                        name: "Utilities",
                        loc: 1700,
                        aaf: 34,
                        correction: {
                            3: 1,
                            10: 2,
                            11: 2,
                            12: 2
                        }
                    }
                ],
                correction: {
                    1: 2,
                    2: 2,
                    4: 2,
                    5: 2,
                    6: 2,
                    7: 2,
                    8: 2,
                    9: 2,
                    13: 2,
                    14: 4,
                    15: 2
                }
            }
        ],
        mode: 'S',
        size: 1
    },methods: {
        addSubsystem: function () {
            this.subsystems.push({
                name: this.newsubsystem.name,
                modules: [],
                correction: {
                    1: 2,
                    2: 2,
                    4: 2,
                    5: 2,
                    6: 2,
                    7: 2,
                    8: 2,
                    9: 2,
                    13: 2,
                    14: 2,
                    15: 2
                }
            });
            this.newsubsystem = {'name': ''};
        },
        
        addModule: function(subsystem){
            this.subsystems[subsystem].modules.push({
                name: this.newmodule.name,
                aaf: 100,
                loc: 0,
                correction: {
                    3: 2,
                    10: 2,
                    11: 2,
                    12: 2
                }
            });

            this.newmodule = {'name': ''};
        },

        subsystemLOC: function (subsystem) {
            var i = 0;
            subsystem.modules.forEach(module => {
                i += parseInt(module.loc) || 0;
            });
            return i;
        },
        
        yieldDistribution: function(phase){
            return data.yieldDistribution[this.mode][this.size][phase];
        },

        yieldPercent: function(phase){
            return Math.round(100 * this.yieldDistribution(phase));
        },

        modulePMnominal: function(module){
            return module.loc / this.YieldNominal;
        },

        modulePMnominal_phase: function(phase, module){
            switch(phase){
                case 0: phase = this.yieldDistributionDP; break;
                case 1: phase = this.yieldDistributionDD; break;
                case 2: phase = this.yieldDistributionCT; break;
                case 3: phase = this.yieldDistributionIT; break;
            }
            return phase * this.modulePMnominal(module);
        },

        modulePMnominal_DP: function(module) {
            return this.modulePMnominal_phase(0, module);
        },

        modulePMnominal_DD: function(module) {
            return this.modulePMnominal_phase(1, module);
        },

        modulePMnominal_CT: function(module) {
            return this.modulePMnominal_phase(2, module);
        },

        modulePMnominal_IT: function(module) {
            return this.modulePMnominal_phase(3, module);
        },

        moduleEM: function(phase, module, question){
            return data.questionPhaseFactor[phase][question][module.correction[question]]||1;
        },

        moduleEM_DP: function(module, question){
            return this.moduleEM(0, module, question);
        },

        moduleEM_DD: function (module, question) {
            return this.moduleEM(1, module, question);
        },

        moduleEM_CT: function (module, question) {
            return this.moduleEM(2, module, question);
        },

        moduleEM_IT: function (module, question) {
            return this.moduleEM(3, module, question);
        },

        moduleEAF: function(phase, module){
            return this.moduleEM(phase, module, 3)
                * this.moduleEM(phase, module, 10)
                * this.moduleEM(phase, module, 11)
                * this.moduleEM(phase, module, 12)
            ;
        },

        modulePMestimated: function(phase, module){
            return this.modulePMnominal_phase(phase, module) * this.moduleEAF(phase, module);
        },

        subsystemPM: function(phase, subsystem){
            var i = 0;
            subsystem.modules.forEach(module => {
                i += this.modulePMestimated(phase, module);
            });

            return i;
        },

        subsystemEM: function(phase, question, subsystem){
            return data.questionsSubsystem[phase][question][subsystem.correction[question]] || 1;
        },

        subsystemEAF: function(phase, subsystem){
            return this.subsystemEM(phase, 1, subsystem)
                * this.subsystemEM(phase, 2, subsystem)
                * this.subsystemEM(phase, 4, subsystem)
                * this.subsystemEM(phase, 5, subsystem)
                * this.subsystemEM(phase, 6, subsystem)
                * this.subsystemEM(phase, 7, subsystem)
                * this.subsystemEM(phase, 8, subsystem)
                * this.subsystemEM(phase, 9, subsystem)
                * this.subsystemEM(phase, 13, subsystem)
                * this.subsystemEM(phase, 14, subsystem)
                * this.subsystemEM(phase, 15, subsystem)
            ;
        },

        subsystemPMEST: function(phase, subsystem){
            return this.subsystemPM(phase, subsystem) * this.subsystemEAF(phase, subsystem);
        },

        l: function(string){
            return language[this.lang][string];
        }
    }
    ,computed: {
        totalLOC: function(){
            var i = 0;
            this.subsystems.forEach(subsystem => {
                subsystem.modules.forEach(module => {
                    i += parseInt(module.loc)||0;
                });
            });

            return i;
        },

        totalKLOC: function(){
            return this.totalLOC / 1000;
        },
        
        PMnominal: function(){
            return data.PMnominalFactors[this.mode][0] * Math.pow(this.totalKLOC, data.PMnominalFactors[this.mode][1]);
        },

        YieldNominal: function(){
            return this.totalLOC / this.PMnominal;
        },

        YieldNominalRounded: function(){
            return Math.round(this.YieldNominal);
        },

        yieldDistributionDP: function(){
            return this.yieldDistribution(0) + this.yieldDistribution(1);
        },

        yieldDistributionDD: function(){
            return this.yieldDistribution(2);
        },

        yieldDistributionCT: function(){
            return this.yieldDistribution(3);
        },

        yieldDistributionIT: function(){
            return this.yieldDistribution(4);
        },

        estimatedPM: function () {
            var i = 0;
            this.subsystems.forEach(subsystem => {
                i += this.subsystemPMEST(0, subsystem)
                    + this.subsystemPMEST(1, subsystem)
                    + this.subsystemPMEST(2, subsystem)
                    + this.subsystemPMEST(3, subsystem)
                ;
            });
            return i;
        },

        YieldEstimated: function(){
            return this.totalLOC / this.estimatedPM;
        },

        YieldEstimatedRounded: function(){
            return Math.round(this.YieldEstimated);
        }
    }
});


window.onbeforeunload = function () {
    return '';
}